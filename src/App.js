import React, { Component } from 'react';
import Bouton from "./Components/Bouton_Profil/Bouton_Profil.js";
import Button from "./Components/Button_Super/Button_Super.js";
import Switch from "./Components/Switch_Color/Switch_Color.js";
import "./App.css";


class App extends Component {

 
    state = {
        prenom: "Jeanne",
        nom: "Jean",
        date: "17/10/1975",
        content: "Bonjour, je m'appelle Jeanne ! 🌼",
          source: require("./images/chat.jpg"),
          alt: "Chat Jeanne",
          color:'lightpink',
      counterM: 0,
      counterJ:0,
      counterC:0,
      colorM:'pink',
      colorJ:'pink',
      colorC:'pink',
    
      }
      

  changeProfil1 = () => {
    this.setState({
        prenom: "Jeanne",
       nom: "Jean",
       date: "17/10/1975",
       content: "Bonjour, je m'appelle Jeanne ! 🌼",
       source: require("./images/chat.jpg"),
       alt: "Chat Jeanne",
      
   }
   ); 
  }

  changeProfil2 = () => {
     this.setState({
          prenom: "Martine",
          nom: "Martin",
          date: "25/08/1937",
          content: "J'aime le Reactjs 😍",
          source: require("./images/quokka.jpg"),
          alt: "Quokka Martine",

    });

   }

   changeProfil3 = () => {
    this.setState({
        prenom: "Claude",
         nom: "Claudine",
         date: "04/04/1944",
         content: "Claude est un très beau prénom. 😎",
         source: require("./images/husky.jpg"),
         alt: "Husky Martine",
      
   });
  }

  increment = () =>{
    if(this.state.prenom === "Jeanne")
      {
        this.setState({
          counterJ : this.state.counterJ + 1
        });  
      }
      if(this.state.prenom === "Claude")
      {
        this.setState({
          counterC : this.state.counterC + 1
        });  
      } 
      if(this.state.prenom === "Martine")
      {
        this.setState({
          counterM : this.state.counterM + 1
        });  
      }        
  }



  afficherCounter = () => {
    if(this.state.prenom === "Jeanne")
      {
        return this.state.counterJ;
      }
      if(this.state.prenom === "Claude")
      {
        return this.state.counterC;
      } 
      if(this.state.prenom === "Martine")
      {
        return this.state.counterM;
      } 
  }


   changeColor = () => {  
       var letters = '0123456789ABCDEF';
	    var colory = '#';
	    for (var i = 0; i < 6; i++ ) {
		  colory += letters[Math.floor(Math.random() * 16)];
      }
	    this.setState({color: colory});
  }
  
  render() {

    return (
      <div>
        <Bouton changeProfil3={this.changeProfil3} changeProfil2={this.changeProfil2} changeProfil1={this.changeProfil1}/>
        <br/>  <br/><br/> <br/> 

        <div className="profil" style={{background: this.state.color}}>
          <img className="img" src={this.state.source} alt={this.state.alt}/>
              <p className="prenom">{this.state.prenom}</p>
              <p className="nom">{this.state.nom} </p>
              <br/><br/><br/>
          <p>{this.state.date}</p>
           <Switch changeColor={this.changeColor}/>  
        </div>
        <div className="super"> 
          <p>{this.state.content}</p>
          <Button increment={this.increment} />
          <p>{this.afficherCounter()}</p>
        </div>

      </div>
    );
  }
}
export default App;
